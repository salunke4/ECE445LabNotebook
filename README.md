# Lab Notebook - Fire Detection and Suppression System

## Team Members
- Arjun Swamy (arswamy2@illinois.edu)
- John Truong (jtruon9@illinois.edu)
- Prathamesh Salunke (salunke4@illinois.edu)

## Overview
This lab notebook provides a detailed record of the individual and collaborative efforts in the development of the Fire Detection and Suppression System for Electric Ranges. Regular meetings were held on Mondays from 9/11 to 12/4, with additional sessions on some Sundays. The final week also involved collaborative work on the project presentation.

## Entry 1 - 9/11

### Objectives:
- Review and discuss the project proposal.
- Define the main ideas and goals of the project.

### Work Done:
I reviewed the project proposal and engaged in a thorough discussion to outline the main objectives of the Fire Detection and Suppression System. Preliminary brainstorming sessions were conducted to identify potential subsystems and their functionalities.

### References:
- https://www.prolinerangehoods.com/blog/cooking-fire-statistics-safety-tips/

## Entry 2 - 9/18

### Objectives:
- Further refine the project goals.
- Begin initial design discussions for subsystems.

### Work Done:
I continued the discussion on the main goals of the project, delving into further refinement. Initial brainstorming sessions for subsystems, with my focus on the sensing subsystem, were initiated. Preliminary requirements for the sensing subsystem were defined.

### Figures and Diagrams:
![Alt text](images/Screenshot_2023-10-25_221233.png)


### References:
https://www.digikey.com/en/products/detail/espressif-systems/ESP32-S3-WROOM-1-N4/16162639

## Entry 3 - 9/25

### Objectives:
- Finalize the design of the sensing subsystem.
- Allocate tasks among team members.

### Work Done:
I concluded the design discussions for the sensing subsystem, reaching a consensus on the final design. Specific tasks were assigned to team members, with a focus on individual responsibilities. A tentative timeline for the project was discussed.

### References:
https://www.digikey.com/en/products/detail/ams-osram-usa-inc/AS7331-T-OLGA16-LF-T-RDP/16983752
https://www.digikey.com/en/products/detail/excelitas-technologies/tpis-1s-1385-5029/9760638


## Entry 4 - 10/2

### Objectives:
Begin coding the functionality of the sensing subsystem.
Investigate suitable sensors for heat detection.

### Work Done:
I initiated the coding process for reading data from IR sensors. Research was conducted to select appropriate IR sensors based on precision requirements. Potential challenges in coding and sensor integration were discussed.

### References:
https://www.digikey.com/short/3r9ffcrj
https://github.com/adafruit/Adafruit-MLX90614-Library
https://gitlab.engr.illinois.edu/arswamy2/445-design-project/-/blob/main/sensing_subsystem.cpp

## Entry 5 - 11/13

### Objectives:
Conduct initial tests of the sensing subsystem.
Address any issues identified during testing.

### Work Done:
I set up experimental conditions to test the accuracy of IR sensors. Initial test parameters, including sensor readings and expected values, were recorded. Discrepancies and potential improvements for the sensing subsystem were identified.

## Entry 6 - 11/20

### Objectives:
Address and resolve issues with I2C connections.
Continue refining the code for the sensing subsystem.

### Work Done:
I investigated and resolved the problem regarding switching the I2C adress for the second sensor. Instead of switching I2C adress, I used i2c.h library from Espressif IDF to establish connection with both IR sensors. Code was updated to improve accuracy and reliability. Additional testing was conducted to verify improvements.

### References:
https://idf.espressif.com/

## Entry 7 - 12/4

### Objectives:
Work on the presentation for the project.

#### Work Done:
Collaborative work on the project presentation was undertaken, incorporating key findings and results. Ensured all documentation aligns with the course requirements.




